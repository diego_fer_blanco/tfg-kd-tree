//
// Created by diego on 16/04/2021.
//
#define TFG_KD_TREE_KD_TREE_HPP_VERBOSE
#define R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP_VERBOSE
#include <r2bt-mod/kd_tree.hpp>
#include <vector>
#include <ctime>
#include <random>
#include <iostream>
#include <iomanip>
#define DIM 2
/*Test para comprobar el arbol generado con un datasheet*/

std::vector<tfg::kd_tree_point<DIM>> gen_k_random_points(unsigned k){
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(-5.0,5.0);
    std::vector<tfg::kd_tree_point<DIM>> result; result.reserve(k);
    for(unsigned i = 0; i < k; i++)
        result.push_back({{distribution(generator), distribution(generator)}});
    return result;
}
int main(){
    using namespace tfg;

    // Querying points
    std::vector<kd_tree_point<DIM>> searching_points = gen_k_random_points(1000000UL);
    kd_tree<DIM> kdTree(searching_points.size());
    kd_tree_point<DIM> search_point = {{3.4, 4.02}};
    kd_tree_point<DIM> result;
    std::vector<kd_tree_point<DIM>> results;
    double radius = 0.2;
    for(kd_tree_point<DIM> point: searching_points)
        kdTree.add(point);
    double t1 = clock();
    kdTree.build();
    double t2 = clock();
//    std::cout << "Arbol generado: \n";
//    std::cout << kdTree << "\n";
//    std::cout << "Fin impresion del arbol.\n";
    std::cout << "Ha tardado " << std::setprecision(5) << (double) double(t2-t1)/CLOCKS_PER_SEC <<" en montarse.\n";

    std::cout << "Busqueda del punto mas cercano al " << search_point << " con un radio maximo de " << radius << ":\n";
    t1 = clock();
    kdTree.search(search_point, radius, result);
    t2 = clock();
    std::cout << "El punto mas cercano ha sido el " << result << "\nHa tardado " << std::setprecision(5) <<  (double) double(t2-t1)/CLOCKS_PER_SEC << " s en encontrarse.\n";

    std::cout << "Busqueda de los puntos mas cercano al " << search_point << " en un radio de " << radius << ":\n";
    t1 = clock();
    kdTree.search_in_radius(search_point, radius, results);
    t2 = clock();
    std::cout << "Ha encontrado "<< results.size() <<" puntos\n";
    std::cout << "Ha tardado " << std::setprecision(5) << (double) double(t2-t1)/CLOCKS_PER_SEC << " s en encontrarlos.\n";
    return 0;
}

