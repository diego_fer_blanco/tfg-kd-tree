//
// Created by srblanco on 7/5/21.
//

#define TFG_KD_TREE_KD_TREE_HPP_VERBOSE
//#define TFG_KD_TREE_KD_TREE_HPP_VERBOSE_LOG
#define R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP_VERBOSE
#include <r2bt-mod/kd_tree.hpp>
#include <r2bt-mod/sol_kd_tree.hpp>
#include <vector>
#include <ctime>
#include <random>
#include <iostream>
#include <iomanip>
#define DIM 2

std::vector<tfg::kd_tree_point<DIM>> gen_k_random_points(unsigned k, double min, double max){
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(min,max);
    std::vector<tfg::kd_tree_point<DIM>> result; result.reserve(k);
    for(unsigned i = 0; i < k; i++)
        result.push_back(tfg::kd_tree_point<DIM>({distribution(generator), distribution(generator)}));
    return result;
}

int main(){
//    std::vector<tfg::kd_tree_point<DIM>> small_set = {
//            {{1.0,5.0}},
//            {{0.0,6.0}},
//            {{5.0,9.0}},
//            {{0.0,0.0}},
//            {{0.0,7.0}},
//            {{2.0,6.0}},
//            {{3.0,10.0}},
//            {{2.0,2.0}},
//            {{5.0,1.0}},
//            {{2.0,3.0}},
//            {{5.0,1.0}},
//            {{3.0,4.0}},
//            {{6.0,3.0}},
//            {{4.0,4.0}},
//            {{5.0,6.0}},
//            {{3.0,2.0}}
//    };

    std::vector<tfg::kd_tree_point<DIM>> big_set = gen_k_random_points(600, -5.0, 5.0);
    tfg::kd_tree<DIM> arbol_no_solapado(big_set.size());
    tfg::sol_kd_tree<DIM> arbol_solapado(big_set.size());
    double radius = 1.0;
    double t1, t2;
    tfg::kd_tree_point<DIM> searching_point = tfg::kd_tree_point<DIM>({-1.0, 3.0}), result_no_sol, result_sol;
    for (const tfg::kd_tree_point<DIM>& p: big_set){
        arbol_no_solapado.add(p);
        arbol_solapado.add(p);
    }
    double t1_no_sol = clock();
    arbol_no_solapado.build();
    double t2_no_sol = clock();

    std::cout << "El arbol no solapado ha tardado en construirse "
    << std::setprecision(5) << (double) double(t2_no_sol-t1_no_sol)/CLOCKS_PER_SEC <<" s\n";
    std::cout << "Busqueda del punto mas cercano al " << searching_point << " con un radio maximo de " << radius << ":\n";
    t1 = clock();
    arbol_no_solapado.search(searching_point, radius, result_no_sol);
    t2 = clock();
    std::cout << "El punto mas cercano ha sido el " << result_no_sol << " (no solapado)\nHa tardado " << std::setprecision(5) <<  (double) double(t2-t1)/CLOCKS_PER_SEC << " s en encontrarse.\n";
    double t1_sol = clock();
    arbol_solapado.build(radius);
    double t2_sol = clock();

    std::cout << "El arbol solapado ha tardado en construirse "
              << std::setprecision(5) << (double) double(t2_sol-t1_sol)/CLOCKS_PER_SEC <<" s\n";
    std::cout << "Hubo un total de " << arbol_solapado.getSize() << " puntos\n";
//    std::cout << "IMPRESIONES DE ARBOLES\n";
//    std::cout << "Arbol no solapado\n";
//    arbol_no_solapado.print_tree();
//    std::cout << "Arbol solapado\n";
//    arbol_solapado.print_tree();
    /*Prueba busqueda de puntos*/

    std::cout << "Busqueda del punto mas cercano al " << searching_point << " con un radio maximo de " << radius << ":\n";
    t1 = clock();
    arbol_solapado.search(searching_point, radius, result_sol);
    t2 = clock();
    std::cout << "El punto mas cercano ha sido el " << result_sol << " (solapado)\nHa tardado " << std::setprecision(5) <<  (double) double(t2-t1)/CLOCKS_PER_SEC << " s en encontrarse.\n";

}
