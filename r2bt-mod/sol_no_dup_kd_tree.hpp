//
// Created by diego on 10/05/2021.
//

#ifndef R2BT_MOD_EXAMPLES_SOL_NO_DUP_KD_TREE_HPP
#define R2BT_MOD_EXAMPLES_SOL_NO_DUP_KD_TREE_HPP
#define R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP_VERBOSE // Para acceder al toString del punto
#include <iostream>
#include <vector>
#include <array>
#include <cmath>
#include <string>
#include "kd_tree_point.hpp"
namespace tfg {
    template<unsigned DIM>
    class sol_no_dup_kd_tree {
    public:
        typedef kd_tree_point<DIM> Point;
        typedef std::vector<int> IndexVector;
        sol_no_dup_kd_tree()(size_t _size) {
            point_vector.reserve(2*_size); //Estimamos el doble de puntos
        }
        void add(const std::array<double, DIM> p) {
            Point new_p(p);
            point_vector.push_back(new_p);
        }
        void build(double radius_hint) {
            IndexVector initial_vector;
            initial_vector.reserve(point_vector.size());
            for (int i = 0; i < (int) point_vector.size(); i++)
                initial_vector.push_back(i); // Vector con los punteros iniciales
            root = build_rec(initial_vector, 0, radius_hint, false);
        }

        void search(Point point, double maximum, Point& result){
            this->searching_point = point;
            search_rec(root, 0, maximum*maximum, maximum, result);
        }
        void search_in_radius(Point point, double radius, std::vector<Point>& results){
            this->searching_point = point;
            search_radius_rec(root,0,radius*radius, radius, results);
        }

        void k_search(Point point, double maximum, std::vector<Point>& result, int k){
            this->searching_point = point;
            k_search_rec(k, root, 0, maximum*maximum, maximum, result);
        }

        size_t getSize(){
            return point_vector.size();
        }
#ifdef TFG_KD_TREE_KD_TREE_HPP_VERBOSE
        void print_tree(){
            this->print_tree_recursive(this->root, 0, std::cout);
            std::cout << "\nNumero de nodos: " << this->getSize() << std::endl;
        }
#endif //TFG_KD_TREE_KD_TREE_HPP_VERBOSE

    private:
        std::vector <Point> point_vector;
        int root = -1;
        Point searching_point;

        int build_rec(IndexVector index_vector, unsigned axis, double radius_hint, bool no_dup) {
            if (index_vector.empty())
                return -1;
            else if(index_vector.size() == 1)
                return index_vector[0];
            int middle_index = (int)floor(index_vector.size() / 2);
            int middle_point_index = index_vector[middle_index];
            IndexVector left_vector;
            left_vector.reserve((size_t) index_vector.size());
            IndexVector right_vector;
            right_vector.reserve((size_t) index_vector.size());
            for(size_t i = 0; i < middle_index; ++i){
                const Point &p = point_vector[index_vector[i]];
                if(p[axis] <= point_vector[middle_point_index][axis]){
                    // Si su axis es menor que el pivote metelo a la izqda
                    left_vector.push_back(index_vector[i]);
                    // Si su axis ademas entra en el rango del radio, implica que podria estar a la dcha
                    if(p[axis] > (point_vector[middle_point_index][axis] - radius_hint)){
                        if(no_dup)
                            right_vector.push_back(index_vector[i]);
                        else{
                            point_vector.push_back(p); // Metelo al point vector
                            right_vector.push_back(point_vector.size() - 1); // Metelo a la dcha
                        }
                    }
                }
                else{
                    // Si su axis es mayor metelo a la dcha
                    right_vector.push_back(index_vector[i]);
                    // Si su axis ademas entra en el rango, implica que podria estar a la izqda
                    if(p[axis] < (point_vector[middle_point_index][axis] + radius_hint)){
                        if(no_dup)
                            left_vector.push_back(index_vector[i]);
                        else {
                            point_vector.push_back(p); // Metelo al point vector
                            left_vector.push_back(point_vector.size() - 1); // Metelo a la izqda
                        }
                    }
                }

            }
            for(size_t i = middle_index + 1; i < index_vector.size(); ++i){
                const Point &p = point_vector[index_vector[i]];
                if(p[axis] <= point_vector[middle_point_index][axis]){
                    // Si su axis es menor que el pivote metelo a la izqda
                    left_vector.push_back(index_vector[i]);
                    // Si su axis ademas entra en el rango del radio, implica que podria estar a la dcha
                    if(p[axis] > (point_vector[middle_point_index][axis] - radius_hint)){
                        if(no_dup)
                            right_vector.push_back(index_vector[i]);
                        else{
                            point_vector.push_back(p); // Metelo al point vector
                            right_vector.push_back(point_vector.size() - 1); // Metelo a la dcha
                        }
                    }
                }
                else{
                    // Si su axis es mayor metelo a la dcha
                    right_vector.push_back(index_vector[i]);
                    // Si su axis ademas entra en el rango, implica que podria estar a la izqda
                    if(p[axis] < (point_vector[middle_point_index][axis] + radius_hint)){
                        if(no_dup)
                            left_vector.push_back(index_vector[i]);
                        else {
                            point_vector.push_back(p); // Metelo al point vector
                            left_vector.push_back(point_vector.size() - 1); // Metelo a la izqda
                        }
                    }
                }
            }
            if (++axis >= DIM) axis = 0; // Cambiamos de axis

            if(no_dup || compare_vectors(right_vector, left_vector)){
                this->point_vector[middle_point_index].left = build_rec(left_vector, axis, radius_hint, true);
                this->point_vector[middle_point_index].right = build_rec(right_vector, axis, radius_hint, true);
            }
            else{
                this->point_vector[middle_point_index].left = build_rec(left_vector, axis, radius_hint, false);
                this->point_vector[middle_point_index].right = build_rec(right_vector, axis, radius_hint, false);
            }


            return middle_point_index;
        }

        bool compare_vectors(IndexVector iv1, IndexVector iv2){
            if(iv1.size() != iv2.size())
                return false;
            for(size_t i = 0; i < iv1.size(); ++i){
                if(point_vector[iv1[i]] != point_vector[iv2[i]])
                    return false;
            }
            return true;
        }


        inline double distance_between_no_sqr(Point p1, Point p2){
            double d = 0;
            for(int i = 0; i < DIM; ++i)
                d += pow(p2[i] - p1[i], 2.0);
            return d;
        }

        void search_rec(int index, unsigned axis, double best_radius, double best_sq_radius, Point& result){
            if(index == -1) return;
            Point& current_point = point_vector[index];
            double distance = distance_between_no_sqr(current_point, searching_point);
            double distance_axis = current_point[axis] - searching_point[axis];
            if(distance < best_radius){
                best_radius = distance;
                best_sq_radius = sqrt(best_radius);
                result = current_point;
            }
            if (++axis >= DIM) axis = 0; // Cambiamos de axis
            if(distance_axis > 0){
                search_rec(current_point.left, axis, best_radius, best_sq_radius, result);
            }
            else{
                search_rec(current_point.right, axis, best_radius, best_sq_radius, result);
            }
        }

        void search_radius_rec(int index, unsigned axis, double best_radius, double best_sq_radius, std::vector<Point>& results){
            if(index == -1) return;
            Point& current_point = point_vector[index];
            double distance = distance_between_no_sqr(current_point, searching_point);
            double distance_axis = current_point[axis] - searching_point[axis];
            if(distance < best_radius){
                results.push_back(current_point);
            }
            if (++axis >= DIM) axis = 0; // Cambiamos de axis
            if(distance_axis > 0){
                search_radius_rec(current_point.left, axis, best_radius, best_sq_radius, results);
            }
            else{
                search_radius_rec(current_point.right, axis, best_radius, best_sq_radius, results);
            }
        }

        void k_search_rec(int k, int index, unsigned axis, double best_radius,
                          double best_sq_radius, std::vector<Point>& result){
            if(index == -1) return;
            Point& current_point = point_vector[index];
            double distance = distance_between_no_sqr(current_point, searching_point);
            double distance_axis = current_point[axis] - searching_point[axis];
            if(distance < best_radius){
                rearrange_array(k, current_point, result, best_radius, best_sq_radius);
            }
            if (++axis >= DIM) axis = 0; // Cambiamos de axis
            if(distance_axis > 0){
                k_search_rec(k, current_point.left, axis, best_radius, best_sq_radius, result);
            }
            else{
                k_search_rec(k, current_point.right, axis, best_radius, best_sq_radius, result);
            }

        }

        void rearrange_array(int k, Point& point, std::vector<Point>& array, double& best_radius, double& best_sq_raduis){
            double distance = distance_between_no_sqr(point, searching_point);
            if(array.size() >= k){
                array.pop_back(); // Nos cargamos el ultimo elemento
            }
            /*
             * MOTIVOS POR LOS QUE UTILIZAR VECTORES
             * Al tratarse de un vector, los elementos estan contiguos en memoria siempre que haya sido prealocado
             * correctamente, por lo que debido a que tendremos el vector muy presente en la cache,
             * los accesos seran rapidos.
             * El inconveniente es que insertar un nuevo elemento en un sitio intermedio, es una
             * operacion costosa, pero los accesos a menor coste merecen la pena*/
            for(int i = 0; i < array.size(); ++i){
                if(distance <= distance_between_no_sqr(array[i], searching_point)){ // Si el nuevo punto esta mas cerca, se le mete en esta posicion
                    array.insert(i, point); // Lo metemos en esa posicione de la i
                    best_radius = distance_between_no_sqr(array.back(), searching_point); // El radio de busqueda = dist punto mas alejado
                    best_sq_raduis = sqrt(best_radius);
                    return;
                }
            }
            array.push_back(point); // Si es el mayor de todos, lo ponemos al final
            best_radius = distance;
            best_sq_raduis = sqrt(best_radius);
        }


#ifdef TFG_KD_TREE_KD_TREE_HPP_VERBOSE

        void print_tree_recursive (int index, int depth, std::ostream & out){
            if ( index == -1 ) return;
            out << point_vector[index] <<"\n";
            int i;
            if( point_vector[index].right != -1 ){
                for ( i = depth+1; i > 0; i-- ) out << "\t";
                out << "Right: ";
                print_tree_recursive ( point_vector[index].right, depth+1, out);
            }
            if( point_vector[index].left != -1 ){
                for ( i = depth+1; i > 0; i-- ) out << "\t";
                out << "Left: ";
                print_tree_recursive ( point_vector[index].left, depth+1, out);
            }
        }
#endif //TFG_KD_TREE_KD_TREE_HPP_VERBOSE
    };
} //namespace


#endif //R2BT_MOD_EXAMPLES_SOL_NO_DUP_KD_TREE_HPP
