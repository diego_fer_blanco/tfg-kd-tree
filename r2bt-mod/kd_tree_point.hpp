//
// Created by diego on 16/04/2021.
//
#include <iostream>
#include <iomanip>
#ifndef R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP
#define R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP
namespace tfg {
    template<unsigned DIM>
    class kd_tree_point
            : public std::array<double, DIM> { //Heredamos las propiedades del array, para tener tres posiciones en memoria contiguos
    public:
        kd_tree_point(){
            for (unsigned i = 0; i < DIM; i++)
            (*this)[i] = 0.0;
        }

        kd_tree_point(const std::array<double, DIM> p) {
            for (unsigned i = 0; i < DIM; i++)
                (*this)[i] = p[i]; //*this es el contenido de la clase que al haber heredado de un array, son DIM doubles
        }

        kd_tree_point(const kd_tree_point<DIM>& new_p) { // Constructor copia de puntos
            // Razones por las que no usar memcpy -> Llamar al SO tarda mucho, y esta llamada se va a hacer muchas veces
            for (unsigned i = 0; i < DIM; i++)
                (*this)[i] = new_p[i];
            this->left = new_p.left;
            this->right = new_p.right;
        }
        friend inline bool operator==(const kd_tree_point<DIM>& lpoint, const kd_tree_point<DIM>& rpoint){
            for(unsigned i = 0; i < DIM; ++i){
                if(lpoint[i] != rpoint[i])
                    return false;
            }
            return true;
        }
        friend inline bool operator!=(const kd_tree_point<DIM>& lpoint, const kd_tree_point<DIM>& rpoint){
            return !(lpoint == rpoint);
        }

        int left = -1, right = -1;
#ifdef R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP_VERBOSE

        friend inline std::ostream &operator<<(std::ostream &Str, kd_tree_point<DIM> point) {
            point.print_point(Str);
            return Str;
        }

    private:
        void print_point(std::ostream &out) {
            out << "[ ";
            for (unsigned i = 0; i < DIM; i++)
                out << std::fixed << std::setprecision(2) << (*this)[i] << " ";
            out << "]";
        }

#endif //R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP_VERBOSE

    };
}
#endif //R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP
