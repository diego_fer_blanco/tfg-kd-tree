//
// Created by diego on 15/04/2021.
//

#ifndef TFG_KD_TREE_KD_TREE_HPP
#define TFG_KD_TREE_KD_TREE_HPP
#define R2BT_MOD_EXAMPLES_KD_TREE_POINT_HPP_VERBOSE // Para acceder al toString del punto
#include <iostream>
#include <vector>
#include <array>
#include <cmath>
#include <string>
#include "kd_tree_point.hpp"
namespace tfg {
    template<unsigned DIM>
    class kd_tree {
    public:
        typedef kd_tree_point<DIM> Point;
        typedef std::vector<int> IndexVector;
        explicit kd_tree(size_t _size) {
            point_vector.reserve(_size);
        }
        void add(const Point p) {
            Point new_p(p);
            point_vector.push_back(new_p);
        }
        void build() {
            IndexVector initial_vector;
            //initial_vector.reserve(point_vector.size());
            for (int i = 0; i < (int) point_vector.size(); i++)
                initial_vector.push_back(i); // Vector con los punteros iniciales
            root = build_rec(initial_vector, 0);
        }

        void k_search(Point point, double maximum, std::vector<Point>& result, int k){
            this->searching_point = point;
            k_search_rec(k, root, 0, maximum*maximum, maximum, result);
        }
        void search(Point point, double maximum, Point& result){
            this->searching_point = point;
            search_rec(root, 0, maximum*maximum, maximum, result);
        }
        void search_in_radius(Point point, double radius, std::vector<Point>& results){
            this->searching_point = point;
            search_radius_rec(root,0,radius*radius, radius, results);
        }
        size_t getSize(){
            return point_vector.size();
        }
#ifdef TFG_KD_TREE_KD_TREE_HPP_VERBOSE
        void  print_tree() {
            this->print_tree_recursive(this->root, 0, std::cout);
            std::cout << "\nNumero de nodos: " << this->getSize() << std::endl;
        }
#endif //TFG_KD_TREE_KD_TREE_HPP_VERBOSE

    private:
        std::vector <Point> point_vector;
        int root = -1;
        Point searching_point;

        int build_rec(IndexVector index_vector, unsigned axis) {
            if (index_vector.empty()) return -1;
            else if(index_vector.size() == 1)
                return index_vector[0];
            auto middle_index = (size_t)floor(index_vector.size() / 2);
            int middle_point_index = index_vector[middle_index];
            IndexVector left_vector;
            left_vector.reserve((size_t) index_vector.size() / 2);
            IndexVector right_vector;
            right_vector.reserve((size_t) index_vector.size() / 2);
            for(size_t i = 0; i < middle_index; ++i){
                if (point_vector[index_vector[i]][axis] <= point_vector[middle_point_index][axis]) // Si es menor o igual va a la izqda
                    left_vector.push_back(index_vector[i]);
                else
                    right_vector.push_back(index_vector[i]); // Si es mayor va a la derecha
            }
            for(size_t i = middle_index + 1; i < index_vector.size(); ++i){
                if (point_vector[index_vector[i]][axis] <= point_vector[middle_point_index][axis]) // Si es menor o igual va a la izqda
                    left_vector.push_back(index_vector[i]);
                else
                    right_vector.push_back(index_vector[i]); // Si es mayor va a la derecha
            }
            if (++axis >= DIM) axis = 0; // Cambiamos de axis
            this->point_vector[middle_point_index].left = build_rec(left_vector, axis);
            this->point_vector[middle_point_index].right = build_rec(right_vector, axis);

            return middle_point_index;
        }

        inline double distance_between_no_sqr(Point p1, Point p2){
            double d = 0;
            for(unsigned i = 0; i < DIM; ++i)
                d += pow(p2[i] - p1[i], 2.0);
            return d;
        }


        void search_rec(int index, unsigned axis, double best_radius, double best_sq_radius, Point& result){
            if(index == -1) return;
            Point& current_point = point_vector[index];
            double distance = distance_between_no_sqr(current_point, searching_point);
            double distance_axis = current_point[axis] - searching_point[axis];
            if(distance < best_radius){
                best_radius = distance;
                best_sq_radius = sqrt(best_radius);
                result = current_point;
            }
            axis = (axis+1)%DIM;
            if(distance_axis > 0){
                search_rec(current_point.left, axis, best_radius, best_sq_radius, result);
                if(best_sq_radius > distance_axis)
                    search_rec(current_point.right, axis, best_radius, best_sq_radius, result);
            }
            else{
                search_rec(current_point.right, axis, best_radius, best_sq_radius, result);
                if(best_sq_radius > -distance_axis)
                    search_rec(current_point.left, axis, best_radius, best_sq_radius, result);
            }
        }

        void search_radius_rec(int index, unsigned axis, double best_radius, double best_sq_radius, std::vector<Point>& results){
            if(index == -1) return;
            Point& current_point = point_vector[index];
            double distance = distance_between_no_sqr(current_point, searching_point);
            double distance_axis = current_point[axis] - searching_point[axis];
            if(distance < best_radius){
                results.push_back(current_point);
            }
            axis = (++axis)%DIM;
            if(distance_axis > 0){
                search_radius_rec(current_point.left, axis, best_radius, best_sq_radius, results);
                if(best_sq_radius > distance_axis)
                    search_radius_rec(current_point.right, axis, best_radius, best_sq_radius, results);
            }
            else{
                search_radius_rec(current_point.right, axis, best_radius, best_sq_radius, results);
                if(best_sq_radius > -distance_axis)
                    search_radius_rec(current_point.left, axis, best_radius, best_sq_radius, results);
            }
        }

        void k_search_rec(int k, int index, unsigned axis, double best_radius,
                          double best_sq_radius, std::vector<Point>& result){
            if(index == -1) return;
            Point& current_point = point_vector[index];
            double distance = distance_between_no_sqr(current_point, searching_point);
            double distance_axis = current_point[axis] - searching_point[axis];
            if(distance < best_radius){
                rearrange_array(k, current_point, result, best_radius, best_sq_radius);
            }
            axis = (axis+1)%DIM;
            if(distance_axis > 0){
                k_search_rec(k, current_point.left, axis, best_radius, best_sq_radius, result);
                if(best_sq_radius > distance_axis)
                    k_search_rec(k, current_point.right, axis, best_radius, best_sq_radius, result);
            }
            else{
                k_search_rec(k, current_point.right, axis, best_radius, best_sq_radius, result);
                if(best_sq_radius > -distance_axis)
                    k_search_rec(k, current_point.left, axis, best_radius, best_sq_radius, result);
            }

        }
        void rearrange_array(int k, Point& point, std::vector<Point>& array, double& best_radius, double& best_sq_raduis){
            double distance = distance_between_no_sqr(point, searching_point);
            if(array.size() >= k){
                array.pop_back(); // Nos cargamos el ultimo elemento
            }
            /*
             * MOTIVOS POR LOS QUE UTILIZAR VECTORES
             * Al tratarse de un vector, los elementos estan contiguos en memoria siempre que haya sido prealocado
             * correctamente, por lo que debido a que tendremos el vector muy presente en la cache,
             * los accesos seran rapidos.
             * El inconveniente es que insertar un nuevo elemento en un sitio intermedio, es una
             * operacion costosa, pero los accesos a menor coste merecen la pena*/
            for(int i = 0; i < array.size(); ++i){
                if(distance <= distance_between_no_sqr(array[i], searching_point)){ // Si el nuevo punto esta mas cerca, se le mete en esta posicion
                    array.insert(i, point); // Lo metemos en esa posicione de la i
                    best_radius = distance_between_no_sqr(array.back(), searching_point); // El radio de busqueda = dist punto mas alejado
                    best_sq_raduis = sqrt(best_radius);
                    return;
                }
            }
            array.push_back(point); // Si es el mayor de todos, lo ponemos al final
            best_radius = distance;
            best_sq_raduis = sqrt(best_radius);
        }


#ifdef TFG_KD_TREE_KD_TREE_HPP_VERBOSE

         void print_tree_recursive (int index, int depth, std::ostream & out){
            if ( index == -1 ) return;
            out << point_vector[index] <<"\n";
            int i;
            if( point_vector[index].right != -1 ){
                for ( i = depth+1; i > 0; i-- ) out << "\t";
                out << "Right: ";
                print_tree_recursive ( point_vector[index].right, depth+1, out);
            }
            if( point_vector[index].left != -1 ){
                for ( i = depth+1; i > 0; i-- ) out << "\t";
                out << "Left: ";
                print_tree_recursive ( point_vector[index].left, depth+1, out);
            }
        }
#endif //TFG_KD_TREE_KD_TREE_HPP_VERBOSE
    };
} //namespace
#endif //TFG_KD_TREE_KD_TREE_HPP
